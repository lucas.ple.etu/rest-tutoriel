package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.DetailDto;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();

    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
    	return TauxTva.NORMAL.taux;
    }
    
    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
    	try {
    		return TauxTva.valueOf(niveau.toUpperCase()).taux;
    	}
    	catch(Exception e) {
    		throw new NiveauTvaInexistantException();
    	}
    }
    
    @GET
    @Path("{niveau}")
    public double getMontantTotal(@PathParam("niveau") String niveau, @QueryParam("somme") String nombre) {
    	try {
    	return Double.parseDouble(nombre) + (getValeurTaux(niveau)/100)*Double.parseDouble(nombre);
    	}
    	catch(NumberFormatException e) {
    		throw e;
    	}
    }
    
    @GET
    @Path("lestaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getInfoTaux() {
    	ArrayList<InfoTauxDto> result = new ArrayList<>();
    	for(TauxTva t : TauxTva.values()) {
    		result.add(new InfoTauxDto(t.name(), t.taux));
    	}
    	return result;
    }
    
    @GET
    @Path("details/{niveau}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public DetailDto getDetail(@PathParam("niveau") String niveau, @QueryParam("somme") String nombre) {
    	double somme;
    	try {
    		somme = Double.parseDouble(nombre);
    		return new DetailDto(TauxTva.valueOf(niveau.toUpperCase()), somme);
    	}
    	catch(NumberFormatException n) {
    		throw n;
    	}
    	catch(Exception e) {
    		throw new NiveauTvaInexistantException();
    	}
    }
}
